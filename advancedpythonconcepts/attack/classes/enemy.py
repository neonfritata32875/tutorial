import random


class Enemy:
    hp = 200

    def __init__(self, low, high):
        self.attack_power = {'low': low, 'high': high}

    def get_attack(self):
        return random.randrange(self.attack_power['low'], self.attack_power['high'])

    def get_hp(self):
        return self.hp

