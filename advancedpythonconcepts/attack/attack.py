"""
    Program: Attack
    Author: Matt M
    Copyright: 2019
"""

from advancedpythonconcepts.attack.classes.enemy import Enemy

enemy1 = Enemy(60, 80)
print("You encounter an enemy with", enemy1.get_hp(), "HP!")

player_hp = 260

while player_hp > 0:
    dmg = enemy1.get_attack()
    player_hp = player_hp - dmg

    if player_hp <= 30:
        player_hp = 30

    print("Enemy strikes for", dmg, "points of damage!  Current player HP is", player_hp)

    if player_hp > 30:
        # pass - empty block of code
        continue

    print("You have low health, you've been teleported to the nearest inn!")
    break

