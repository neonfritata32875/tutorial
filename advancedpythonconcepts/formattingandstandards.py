import os
import sys


def my_function(one, two,
                three, four,
                five, six):
    print("hello world")


def second_function():
    print("second function")


my_list = [1, 2,
           3, 4,
           5, 6]

print("Hello ", "Hi")

print(my_list[2])

x = 3 * 52 + 7

if x == 100:
    print("one hundred!")
else:
    print("not one hundred!")

my_function(1,2,3,4,5,6)
second_function()
