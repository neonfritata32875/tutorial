"""
    Program: NeonCalc
    Author: Matthew Metzger
"""
'''What the hell'''

import re

print("Welcome to NeonCalc!")
print("Type 'quit' to exit\n")
previous = 0
run = True


def perform_math():
    global previous
    equation = ""

    if previous == 0:
        equation = input("Enter equation:")
    else:
        equation = input(str(previous))

    if equation == 'quit':
        quit_math()
    else:
        equation = cleanse(equation)

        if previous == 0:
            previous = do_eval(equation)
        else:
            previous = do_eval(str(previous) + equation)


def cleanse(string):
    return re.sub('[a-zA-Z,.:()" "]', '', string)


def do_eval(equation):
    return eval(equation)


def quit_math():
    global run
    run = False
    print("Goodbye!")


while run:
    perform_math()
