# concatenate string
print("Hell " + "world")

# escape characters
print('She said "Don\'t do that"')

# concatenate strings
print("Hello, "+"Matt. ""This costs " + str(6) + " dollars.")

# splitting strings
print("Hello:Matt".split(":"))
print("Hello:Matt".split(":")[0])
print("Hello:Matt".split(":")[1])