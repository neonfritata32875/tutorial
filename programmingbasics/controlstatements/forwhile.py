numbers = [1, 2, 3, 4, 5]
numbersum = 0

for number in numbers:
    numbersum = numbersum+number

print(numbersum)

numbersum = 0

while numbersum < 14:
    numbersum += 1

    if numbersum < 14:
        print("Keep going!")
    else:
        print("Stop!")
