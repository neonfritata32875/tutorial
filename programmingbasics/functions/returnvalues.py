def my_function(name="Matt", age=35):
    return "My name is " + name + " and my age is " + str(age)


greeting1 = my_function("Neon", 100)
greeting2 = my_function("Sigg", 45)

print(greeting1)
print(greeting2)
