print(str(10))
print(int("10"))
print(bool("False"))
print(float("5.4365"))
print(len("try to figure out how many characters there are here!"))
print(len(["first", "second", "third", "fourth"]))
print(sorted(["first", "Second", "third", "fourth"]))
print(sorted([99, 34, 12, 325, 323, 1, 33]))
# sorted order = numbers first, uppercase letters, lowercase letters
