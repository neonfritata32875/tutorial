def my_first_function():
    print("Hello Neon Fritata")


def my_second_function(myinput):
    print("Hello " + myinput)


def my_third_function(myinput, otherinput):
    print(myinput + " says...")
    my_second_function(otherinput)


my_first_function()
my_second_function("Sigg Bilbot")
my_third_function("Sigg Bilbot", "Neon Fritata")

